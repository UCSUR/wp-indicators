<?php
/**
 * Front-Page Template
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */
get_header(); ?>

<div class="content-container">
    <div class="row">
        <div class="small-12 medium-4 large-3 columns">
            <?php if($terms = get_terms('indicator_categories', array('parent' => 0, 'hide_empty' => false))) : ?>
            <div class="wrapper indicators-menu">
                <h3 class="text-center">Know your region</h3>
                <hr>
                <ul class="side-nav">
                    <?php foreach($terms as $term) : ?>
                        <li>
                            <a href="<?php echo get_term_link($term,'indicator_categories'); ?>">
                                <i class="flaticon-<?php echo $term->slug; ?>"></i> <?php echo $term->name; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <br>
            <div class="wrapper whats-new">
                <h3 class="text-center">Whats New</h3>
                <?php if ( $posts = wp_get_recent_posts(array('numberposts' => 3, 'post_type' => 'indicators'), OBJECT) ) : ?>
                    <?php foreach($posts as $post) : setup_postdata($post); ?>
                        <hr>
                        <p class="title"><a href="<?php echo get_the_permalink(); ?>"><?php echo $post->post_title; ?></a></p>
                        <p class="excerpt"><?php echo wp_trim_words(get_the_excerpt(), '30', '...'); ?></p>
                        <?php $cat = wp_get_post_terms($post->ID, 'indicator_categories', array( 'parent' => 0 ))[0]; ?>
                        <p class="category">
                            <a href="<?php echo get_term_link($cat,'indicator_categories'); ?>">
                                <i class="flaticon-<?php echo $cat->slug; ?>"></i> <?php echo $cat->name; ?>
                            </a>
                        </p>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="small-12 medium-8 large-9 columns">
            <?php if ( $posts = wp_get_recent_posts(array('numberposts' => 5, 'meta_key' => '_thumbnail_id'), OBJECT) ) : ?>
            <div class="wrapper owl-carousel">
                <?php foreach($posts as $post) : setup_postdata($post); ?>
                    <?php $img =  wp_get_attachment_image_src(get_post_thumbnail_id(), array('1365','600')); ?>
                    <div class="owl-item">
                        <img src="<?php echo $img[0]; ?>" alt="Slider Image"/>
                        <div class="caption">
                            <h1><?php echo $post->post_title; ?></h1>
                            <p><?php echo wp_trim_words(get_the_excerpt(), '35', ' [...]'); ?></p>
                            <p><a href="<?php the_permalink(); ?>">Read More</a></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <br>
            <?php endif; ?>
            <div class="wrapper">
                <?php if ( $posts = wp_get_recent_posts(array('numberposts' => 9, 'post_type' => 'post'), OBJECT) ) : ?>
                    <ul class="medium-block-grid-2 large-block-grid-3">
                    <?php foreach($posts as $post) : setup_postdata($post); ?>
                        <li class="recent-post">
                            <?php if(has_post_thumbnail()) : ?>
                                <a class="th" href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail('large',''); ?>
                                </a>
                            <?php endif; ?>
                            <h4><?php echo $post->post_title; ?></h4>
                            <div class="text-left">
                                <p><?php echo wp_trim_words(get_the_excerpt(), '20', '...'); ?> <a class="text-center" href="<?php the_permalink(); ?>">More</a></p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                <?php else : ?>
                    <div id="no-posts">
                        <p>Nothing has been posted yet.</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
</body>
</html>