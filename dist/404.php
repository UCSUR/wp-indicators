<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
get_header(); ?>
    <div class="content-container">
        <div class="row">
            <div class="large-12 columns">
                <section id="single-post">
                    <article id="404-page">
                        <h1>404 - Page Not Found</h1>
                        <hr>
                        <p>We're sorry, the page or file you requested was not found:</p>
                        <div class="panel">
                            <p><?php echo esc_url($protocol . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]); ?></p>
                        </div>
                        <p>Perhaps we can help:</p>
                        <ul class="circle">
                            <li>Try using the site-wide search feature in the header.</li>
                            <li>Still stuck? Please <a href="<?php echo get_option('wpindicators_setting_contact_url','#'); ?>">contact us</a> and we'll try our best to help you out.</li>
                        </ul>
                        <br>
                    </article>
                </section>
            </div>
        </div>
    </div>
<?php
get_footer();

