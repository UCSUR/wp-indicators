<?php
/**
 * Main Template
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */
get_header(); ?>

<!-- Posts/Categories -->
<div class="content-container">
    <div class="row">
        <div class="large-12 columns">
            <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-3 data-equalizer">
            <?php while ( have_posts() ) : the_post(); ?>
            <li class="post-grid data-equalizer-watch">
                <article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <h3><?php the_title(); ?></h3>
                    <hr>
                    <?php if ( has_post_thumbnail() ) : ?>
                        <a class="th" href="<?php the_permalink(); ?>">
                            <?php echo get_the_post_thumbnail(get_the_ID(), 'large'); ?>
                        </a>
                        <br>
                    <?php endif; ?>
                    <div class="entry-content">
                        <p>
                            <?php echo wp_trim_words(get_the_excerpt(), '55', '...'); ?>
                            <a class="text-center" href="<?php the_permalink(); ?>">More</a>
                        </p>

                    </div>
                    <br>
                </article>
                </li>
            <?php endwhile; ?>
            </ul>
        </div>
        <?php if ($wp_query->max_num_pages > 1) : ?>
            <div class="large-12 columns">
                <div id="pagination-wrapper" class="single-post hide-for-print">
                    <?php foundation_pagination(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
</body>
</html>