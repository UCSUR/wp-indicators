<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */

// Variables
$search = get_search_query();
$pattern = '/('.$search.')/i';
$replacement = '<strong>$1</strong>';
$search_results = array();

// Logic Outside of HTML
if ( have_posts() ) {
    while ( have_posts() ) {
        the_post();
        $temp = array(
            'id' => get_the_id(),
            'title' => preg_replace($pattern, $replacement, get_the_title()),
            'desc' => preg_replace($pattern, $replacement, get_the_excerpt()),
            'link' => preg_replace($pattern, $replacement, get_the_permalink()),
            'href' => get_the_permalink()
        );
        if ( has_post_thumbnail() ) {
            $temp['thumb'] = get_the_post_thumbnail(get_the_id(), 'medium');
        }
        $search_results[] = $temp;
    }
}
get_header(); ?>
    <!-- Search Results -->
    <div class="content-container">
        <div class="row">
            <div class="large-12 columns">
                <article id="search-results">
                    <?php if ( !empty( $search_results ) ) : ?>
                        <h1>Search Results for: <strong><?php echo $search; ?></strong></h1>
                        <hr>
                        <?php foreach ($search_results as $res) : ?>
                            <h3><a href="<?php echo $res['href']; ?>"><?php echo $res['title']; ?></a></h3>
                            <p>
                                <?php if ( $res['thumb'] ) : ?>
                                    <a class="th left" href="<?php echo the_permalink(); ?>">
                                        <?php echo get_the_post_thumbnail($res['id'], array(225,225)); ?>
                                    </a>
                                <?php endif; ?>
                                <?php echo $res['desc']; ?>
                                <br>
                                <small><a class="subtle-link" href="<?php echo $res['href']; ?>"><?php echo get_the_permalink(); ?></a></small>
                            </p>
                            <hr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <h1>Nothing Found!</h1>
                        <hr>
                        <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
                        <br>
                    <?php endif; ?>
                </article>
            </div>
        </div>
    </div>
<?php
get_footer();