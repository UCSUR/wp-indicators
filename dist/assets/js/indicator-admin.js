jQuery(document).ready(function($){
    //create tabs inside metabox for indicator settings
    $("#mytabs").find(".hidden").removeClass('hidden');
    $("#mytabs").tabs({
        active: 0,
        activate: function(e,ui) {
            ui.oldTab.removeClass('tabs');
            ui.newTab.addClass('tabs');
        }
    });
    //add new measure to indicator
    $('.indicator-add-new-measure').click(function(e) {
        e.preventDefault();
        $('.cloner-measure').clone().removeClass('cloner-measure').show().appendTo('#indicator-measures-table');
    });
    //remove measure from indicator
    $('tbody').on('click', 'a.indicator-remove-measure', function(e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        if(!tr.hasClass('cloner'))
            tr.remove();
    });
    //add new source to indicator
    $('.indicator-add-new-source').click(function(e) {
        e.preventDefault();
        $('.cloner-source').clone().removeClass('cloner-source').show().appendTo('#indicator-sources-table');
    });
    //remove source from indicator
    $('tbody').on('click', 'a.indicator-remove-source', function(e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        if(!tr.hasClass('cloner'))
            tr.remove();
    });
    $(document).ready(function() {
        //trigger indicator category select to retrieve sub-categories via AJAX
        $('select#indicator-category').trigger('change');
    });
    //retireve sub-categories via AJAX when main indicator category changes
    $('select#indicator-category').change(function() {
        var td = $(this).closest('td');
        var sub = td.data('selected-sub');
        var url = td.data('action');
        var val = $(this).val();
        $.ajax({
            type: 'POST',
            url:  url,
            data: 'selected=' + val + '&action=indicator_select_category',
            dataType: 'json',
            cache: false,
            success: function(res) {
                var select =  $('#indicator-sub-category').find('option').remove().end().prop('disabled',false);
                for(var i = 0; i < res.length; i ++) {
                    select.append('<option value="'+res[i].term_id+'"'+(sub == res[i].term_id ? 'selected ' : '')+'>'+res[i].name+'</option>');
                }
            }
        });
    });
});