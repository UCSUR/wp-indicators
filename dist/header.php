<?php
/**
 * Header Template
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_bloginfo('description'); ?></title>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png?v=1">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,700" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/flaticon.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css"/>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr.js"></script>
</head>
<?php wp_head(); ?>
<body>
<!-- Header Bar -->
<div id="header">
    <div class="row">
        <div id="header-left" class="large-4 columns">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <?php if(get_option('wpindicators_setting_medium_logo')) : ?>
                    <img alt="Medium Logo Image" src="<?php echo get_option('wpindicators_setting_medium_logo'); ?>" />
                <?php else : ?>
                    <img alt="Medium Logo Image" src="<?php echo get_template_directory_uri(); ?>/assets/img/medium-logo.png" />
                <?php endif; ?>
            </a>
        </div>
        <div id="header-right" class="large-8 columns hide-for-print">
            <div class="row">
                <div class="large-9 medium-12 columns">
                    <h3 class="slogan text-center large-text-left"><?php echo esc_html(get_bloginfo('description')); ?></h3>
                </div>
                <div class="large-3 medium-12 columns show-for-medium-up">
                    <form role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
                        <div class="row collapse">
                            <div class="small-9 columns">
                                <input type="text" name="s" placeholder="Search">
                            </div>
                            <div class="small-3 columns">
                                <button type="submit" class="button postfix">
                                    <i class="flaticon-magnifying-glass32"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row show-for-large-up hide-for-print">
                <br>
                <div class="large-12 columns">
                    <div class="row collapse">
                        <div class="large-7 columns">
                            <p class="signup-info"><strong>Get Smart!</strong> Sign up to receive regional updates.<p>
                        </div>
                        <div class="large-5 columns">
                            <form class="newsletter" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-abide="ajax">
                                <?php wp_nonce_field('newsletter-register'); ?>
                                <div class="row collapse">
                                    <div class="small-9 columns">
                                        <div class="email-field">
                                            <label for="email">
                                                <input type="email" value="" name="email" placeholder="Your Email Address" required />
                                            </label>
                                            <small class="error">A valid email address is required.</small>
                                        </div>
                                    </div>
                                    <div class="small-3 columns">
                                        <input type="submit" class="button postfix" value="Sign Up" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Navigation Bar -->
<div id="navgiation" class="hide-for-print">
    <div class="row">
        <div class="large-12 columns">
            <nav class="top-bar" data-topbar role="navigation">
                <ul class="title-area">
                    <li class="name"></li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>
                <section class="top-bar-section">
                    <?php
                    wp_nav_menu(array(
                        'container' => false,                           // remove nav container
                        'menu' => '',                      	            // menu name
                        'menu_class' => 'left',         	            // adding custom nav class
                        'theme_location' => 'main-navigation',          // where it's located in the theme
                        'depth' => 3,                                   // limit the depth of the nav
                        'link_before' => '<i></i>',
                        'walker' => new ThemeWalker()
                    ));
                    ?>
                </section>
            </nav>
        </div>
    </div>
</div>
<!-- Main Content -->
<div id="main-content-wrapper">

    <?php if( !is_front_page() && function_exists('bcn_display') && !is_search() ) : ?>
    <div class="row breadcrumbs-container hide-for-small">
        <div class="large-12 columns hide-for-print">
            <article class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                <?php bcn_display(); ?>
            </article>
        </div>
    </div>
    <?php endif; ?>