<?php
/**
 * Footer Template
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */
$tweets = Twitter::getTweets();
?>
</div>
<!-- Footer -->
<div id="footer" class="hide-for-print">
    <div class="row">
        <div class="small-12 large-2 columns">
            <ul class="sidenav-footer">
                <li><a href="http://dcdashboard.org/">Home</a></li>
                <li><a href="http://dcdashboard.org/about-us/">About Us</a></li>
                <li><a href="http://dcdashboard.org/indicators/">Indicators</a></li>
                <li><a href="http://dcdashboard.org/category/special-reports/">Special Reports</a></li>
                <li><a href="http://dcdashboard.org/contact-us/">Contact Us</a></li>
            </ul>
        </div>
        <div class="small-12 large-3 columns">
            <h3>Get Smart!</h3>
            <p>Sign up to receive regional updates.</p>
            <form class="newsletter" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-abide="ajax">
                <?php wp_nonce_field('newsletter-register'); ?>
                <div class="email-field">
                    <label for="email">
                        <input type="email" value="" name="email" placeholder="Your Email Address" required />
                    </label>
                    <small class="error">A valid email address is required.</small>
                </div>
                <input type="submit" class="button secondary small" value="Sign Up" />
            </form>
        </div>
        <div class="small-12 large-3 columns latest-tweets">
            <h3>Latest Tweets</h3>
            <?php if ( $tweets ) : ?>
                <?php foreach($tweets as $k => $tweet) : ?>
                    <?php if($k > 0) : echo '<hr>'; endif; ?>
                    <p class="tweet"><?php echo $tweet; ?></p>
                <?php endforeach; ?>
            <?php else : ?>
                <p class="tweet">An issue has occurred retrieving the latest tweets.</p>
            <?php endif; ?>
        </div>
        <div class="small-12 large-3 columns end">
            <h3>Stay Connected</h3>
            <ul class="small-block-grid-6">
                <?php if(get_option('wpindicators_setting_twitter_username')) : ?>
                    <li>
                        <a href="https://www.twitter.com/<?php echo get_option('wpindicators_setting_twitter_username'); ?>" class="social-link">
                            <i class="flaticon-twitter"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(get_option('wpindicators_setting_facebook_username')) : ?>
                    <li>
                        <a href="https://www.facebook.com/<?php echo get_option('wpindicators_setting_facebook_username'); ?>" class="social-link">
                            <i class="flaticon-facebook2"></i>
                <?php endif; ?>
                <?php if(get_option('wpindicators_setting_github_username')) : ?>
                    <li>
                        <a href="https://www.github.com/<?php echo get_option('wpindicators_setting_github_username'); ?>" class="social-link">
                            <i class="flaticon-github10"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(get_option('wpindicators_setting_googleplus_username')) : ?>
                    <li>
                        <a href="https://plus.google.com/+<?php echo get_option('wpindicators_setting_googleplus_username'); ?>" class="social-link">
                            <i class="flaticon-google-plus"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(get_option('wpindicators_setting_contact_url')) : ?>
                    <li>
                        <a href="<?php echo get_option('wpindicators_setting_contact_url'); ?>" class="social-link">
                            <i class="flaticon-email131"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="<?php bloginfo('rss2_url'); ?>" class="social-link" target="_blank">
                        <i class="flaticon-social5"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- Sub-Footer -->
<div id="sub-footer" class="hide-for-print">
    <div class="row">
        <div class="small-12 columns">
            <p class="right">Powered by <a href="http://ucsur.pitt.edu" target="_blank">UCSUR</a>.</p>
        </div>
    </div>
</div>

<!-- Hidden Modal for Alerts -->
<div id="alertModal" class="reveal-modal" data-reveal aria-labelledby="title" aria-hidden="true" role="dialog">
  <h2 id="ModalTitle"></h2>
  <p id="ModalLead" class="lead"></p>
  <p id="ModalText"></p>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<!-- Begin Javascript -->
<?php if(get_option('wpindicators_setting_google_analytics')) : ?>
<script type="text/javascript">
    jQuery(document).foundation();
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', '<?php echo get_option('wpindicators_setting_google_analytics'); ?>', 'auto');
    ga('send', 'pageview');
</script>
<?php endif; ?>

<?php wp_footer(); ?>