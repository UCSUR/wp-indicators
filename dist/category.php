<?php
/**
 * Category Template
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */
get_header(); ?>

<!-- Posts/Categories -->
<div class="content-container">
    <div class="row">
        <div class="large-12 columns">
            <?php if ( have_posts() ) : ?>
                <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-3" data-equalizer>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <li class="post-grid">
                            <article id="page-<?php the_ID(); ?>" <?php post_class(); ?> data-equalizer-watch>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <hr>
                                <h2>
                                    <?php
                                    $categories = wp_get_post_terms(get_the_ID(), 'indicator_categories');
                                    usort($categories, function($a,$b) { return strcmp($a->parent, $b->parent); });
                                    if($categories) :
                                        ?>
                                        <p class="indicator-categories">
                                            <?php foreach($categories as $key => $cat) : ?>
                                                <?php if($key > 0) : ?>
                                                    <span>/</span>
                                                <?php endif; ?>
                                                <a href="<?php echo get_term_link($cat,'indicator_categories'); ?>">
                                                    <i class="flaticon-<?php echo $cat->slug; ?>"></i> <?php echo $cat->name; ?>
                                                </a>
                                            <?php endforeach; ?>
                                        </p>
                                    <?php endif; ?>

                                </h2>
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <a class="th" href="<?php the_permalink(); ?>">
                                        <?php echo get_the_post_thumbnail(get_the_ID(), 'large'); ?>
                                    </a>
                                    <br>
                                <?php endif; ?>
                                <div class="entry-content">
                                    <p>
                                        <?php echo wp_trim_words(get_the_excerpt(), '55', '...'); ?>
                                        <a class="text-center" href="<?php the_permalink(); ?>">More</a>
                                    </p>

                                </div>
                                <br>
                            </article>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php else : ?>
                <section id="single-post">
                    <h1>Nothing Found!</h1>
                    <hr>
                    <p>Sorry, but nothing has been been posted for <?php echo single_cat_title( '', false ); ?> yet. Try checking back later to see if anything has been posted.</p>
                    <br>
                </section>
            <?php endif; ?>
        </div>
        <?php if ($wp_query->max_num_pages > 1) : ?>
            <div class="large-12 columns">
                <div id="pagination-wrapper" class="single-post hide-for-print">
                    <?php foundation_pagination(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
</body>
</html>