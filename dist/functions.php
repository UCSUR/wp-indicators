<?php
/**
 * Theme Initialization and Setup
 *
 * @since wpindicators 0.1
 */
function theme_init() {
    // Register Custom Navigation Menu
    register_nav_menu('main-navigation', __('Main Navigation', 'wpindicators'));

    // Allow Thumbnails/Featured Images
    add_theme_support( 'post-thumbnails' );

    // Add Custom JS in Footer
    wp_enqueue_script('jquery');
    wp_enqueue_script('foundation', get_template_directory_uri() . '/assets/js/foundation.min.js', array('jquery'), '5.0', true);
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '2.0', true);
    wp_enqueue_script('wpindicators', get_template_directory_uri() . '/assets/js/app.js', array('jquery'), '0.0.1', true);
}
add_action( 'after_setup_theme', 'theme_init' );

/**
 * Create Theme's Custom Settings Page
 *
 * @since wpindicators 0.1
 * @see /scripts/ThemeSettings.php
 */
function theme_settings_add_page() {
    require get_template_directory() . '/scripts/ThemeSettings.php';
    new ThemeSettings();
}
add_action('admin_menu', 'theme_settings_add_page');

/**
 * Temporary enables PHP errors on a single page for enhanced debugging.
 *
 * @since wpindicators 0.1
 */
function enable_php_errors()
{
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
//enable_php_errors();

/**
 * Add custom filter for NavXT Breadcrumbs urls plugin
 *
 * @since wpindicators 0.1
 * @param string $url
 * @return string $url - removes %indicator_categories% from url
 */
function filter_bcn_breadcrumb_url($url)
{
    return str_replace('%indicator_categories%/', '', $url);
};
if(function_exists('bcn_display')) { add_filter('bcn_breadcrumb_url', 'filter_bcn_breadcrumb_url', 10, 1); }

/**
 * Add custom javascript to be included for admin theme-settings page
 *
 * @since wpindicators 0.1
 */
function theme_admin_scripts() {
    if (isset($_GET['page']) && $_GET['page'] == 'theme-settings') {
        wp_enqueue_media();
        wp_enqueue_script('admin-js', get_template_directory_uri() . '/assets/js/admin.js', array('jquery'), '0.0.1', true);
    }
}
add_action('admin_enqueue_scripts', 'theme_admin_scripts');

/**
 * Adds filter to show category list or single category page
 *
 * @return mixed
 */
function subcategory_hierarchy() {
    $query = get_queried_object();

    if ( $query->name == 'indicators' ) {
        $templates = 'archive-indicators.php';
    } else {
        $templates = 'category.php';
    }

    return locate_template($templates);
}

add_filter('archive_template', 'subcategory_hierarchy');
/**
 * Enable Twitter API script to be accessed on scripts
 *
 * @since wpindicators 0.1
 */
require get_template_directory() . '/scripts/Twitter.php';

/**
 * Enable ThemeWalker script to be accessed on scripts
 *
 * @since wpindicators 0.1
 */
require get_template_directory() . '/scripts/ThemeWalker.php';

/**
 * Enable Newsletter script to be accessed via AJAX
 *
 * @since wpindicators 0.1
 * @see /scripts/MailChimp.php
 */
require get_template_directory() . '/scripts/Newsletter.php';

/**
 * Add Indicators Post-Type to WordPress
 *
 * @since wpindicators 0.1
 */
require get_template_directory() . '/scripts/Indicators.php';


/**
 * Add Foundation Styled WordPress Pagination
 *
 * @since wpindicators 0.1
 */
require get_template_directory() . '/scripts/Pagination.php';