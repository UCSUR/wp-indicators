<?php
/**
 * Archive Template for Indicators
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */

$indicator_cats = array();
foreach (get_terms('indicator_categories', array('hide_empty' => 0)) as $term) {
    if ($term->parent == 0) {
        $term->url = get_term_link($term->slug, $term->taxonomy);
        $indicator_cats[] = $term;
    }
}

get_header(); ?>

<!-- Indicator Posts -->
<div class="content-container">
    <div class="row">
        <div class="large-12 columns">
            <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-3" data-equalizer>
                <?php foreach ($indicator_cats as $cat) : ?>
                    <li class="post-grid">
                        <article>
                            <div data-equalizer-watch>
                                <h3>
                                    <a href="<?php echo esc_url($cat->url); ?>">
                                        <i class="flaticon-<?php echo $cat->slug; ?>"></i>
                                        <?php echo $cat->name; ?>
                                    </a>
                                </h3>
                                <hr>
                                <p><?php echo $cat->description; ?></p>
                            </div>
                            <br>
                            <div class="text-center">
                                <a href="<?php echo esc_url($cat->url); ?>" class="button small">
                                    View Indicators
                                </a>
                            </div>
                        </article>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>

<?php get_footer(); ?>
</body>
</html>