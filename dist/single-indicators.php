<?php
    function get_meta($key, $default = '', $esc = true) {
        if($esc)
            echo esc_js(get_post_meta(get_the_ID(), $key, true));
        else
            echo get_post_meta(get_the_ID(), $key, true);
    }
    $checkChart1 = array(
        'imagechart and table',
        'percent change',
        'absolute change',
        'absolute and percent change',
        'value chart percent table',
        'value chart absolute table'
    );
    $checkChart2 = array(
        'barchart and table',
        'columnchart years months',
        'packed columnchart',
        'pgh vs bench by year',
        'month year linechart'
    );
    $checkChart3 = array(
        'raw table',
        'change table',
        'imagechart and table',
        'barchart and table',
        'percent change',
        'absolute change',
        'absolute and percent change',
        'pgh vs bench by year',
        'month year linechart',
        'value chart percent table',
        'value chart absolute table'
    );
    $checkChart4 = array(
        'change table line',
        'raw table line',
        'raw table index line'
    );

    wp_enqueue_script('google-jsapi', 'https://www.google.com/jsapi');
    wp_enqueue_script('indicator-view', get_template_directory_uri() . '/assets/js/indicator-view.js');
    wp_enqueue_script('indicator-num', get_template_directory_uri() . '/assets/js/indicator-numbers.js');

    get_header();

    $viewType = get_post_meta(get_the_ID(), 'indicator_view_type', true);
    $showMeasures = get_post_meta(get_the_ID(), 'indicator_show_measures', true) == 1 ? true : false;
    $spreadsheet = get_post_meta( get_the_ID(), 'indicator_spreadsheet_url', true );

    $categories = wp_get_post_terms(get_the_ID(), 'indicator_categories');
    usort($categories, function($a,$b) { return strcmp($a->parent, $b->parent); });
    $icon = $categories ? $categories[0]->slug : false;
?>
    <div class="content-container">
        <div class="row">
            <div class="large-12 columns">

                <!-- Indicator Content -->
                <section id="single-post">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <article id="page-<?php the_ID(); ?>" class="single-indicator">
                            <h1>
                                <?php if($icon) : ?>
                                    <i class="flaticon-<?php echo $icon; ?>"></i>
                                <?php endif; ?>
                                <?php the_title(); ?>
                            </h1>
                            <hr>
                            <div class="entry-content">
                                <?php the_content(); ?>
                            </div>
                            <br>
                        </article>
                    <?php endwhile; ?>
                </section>

                <br>

                <!-- Indicator Header -->
                <?php if($spreadsheet || $showMeasures) : ?>
                <section id="indicator-header">
                    <div class="row">
                        <div class="medium-7 columns">
                        <div id="indicator-measures" class="hide">
                            <?php if($showMeasures) : ?>
                                <?php if($measures = get_post_meta($post->ID, 'indicator_measures', true)) : ?>
                                    <?php if(!empty(unserialize($measures))) : ?>
                                        <label for="select-measures">Selected measure:</label>
                                        <select id="select-measures" name="measure">
                                            <?php foreach(unserialize($measures) as $key => $array) : ?>
                                                <option value="<?php echo ltrim($key,'m'); ?>"><?php echo $array[0]; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        </div>
                        <div class="medium-5 columns large-text-right small-text-center">
                            <?php if($spreadsheet) : ?>
                            <a href="https://docs.google.com/spreadsheets/d/<?php echo $spreadsheet; ?>" target="_blank">
                                <i class="flaticon-download"></i> View Raw Data
                            </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <?php endif; ?>

                <!-- Indicator Image, Chart, Table, Etc. -->
                <section id="single-indicator">
                    <div class="indicator-error hide">
                        <h2>Error!</h2>
                        <p>An error has occurred when trying to load this indicator. Please try again later, if this issue continues to occur please <a href="<?php echo esc_url(get_option('wpindicators_setting_contact_url')); ?>">contact us</a>.</p>
                        <br>
                    </div>
                    <div class="indicator-loading text-center">
                        <br><br>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/loading.gif" alt="Loading Image" />
                        <br><br>
                    </div>
                    <div class="view_panel">
                        <?php if(in_array($viewType, $checkChart1)) : ?>
                            <div class="chart_column">
                                <div class="datachart" id="chart_div">
                                    <img alt="loading..." id="chart_img" src="<?php echo get_template_directory_uri(); ?>/assets/img/spacer.gif"/>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if(in_array($viewType, $checkChart2)) : ?>
                            <div class="chart_column">
                                <h5 id="chart_title"></h5>
                                <div class="datachart" id="chart_div"></div>
                            </div>
                        <?php endif; ?>
                        <?php if(in_array($viewType, $checkChart3)) : ?>
                            <div class="chart_column">
                                <?php if($viewType == 'raw table' || $viewType == 'change table') : ?>
                                    <h5 id="chart_title"></h5>
                                <?php endif; ?>
                                <div class="datatable" id="table_div"></div>
                            </div>
                        <?php endif; ?>
                        <?php if(in_array($viewType, $checkChart4)) : ?>
                            <div class="table_column">
                                <h5 id="chart_title"></h5>
                                <div id="table2_div"></div>
                                <p id="chartmsg" class="hide">Click a table row to change compared region</p>
                                <div id="table1_div"></div>
                            </div>
                        <?php endif; ?>
                    </div>
                </section>
                <br>
                <?php if($sources = get_post_meta($post->ID, 'indicator_sources', true)) : ?>
                    <?php if(!empty(unserialize($sources))) : ?>
                    <!-- Indicator Sources -->
                    <section id="indicator-sources">
                        <h5>Data Source(s):</h5>
                        <?php foreach(unserialize($sources) as $array) : ?>
                            <?php if(!empty($array[1])) : ?>
                                <p><a href="<?php echo $array[1]; ?>"><?php echo $array[0]; ?></a></p>
                            <?php else : ?>
                                <p><?php echo $array[0]; ?></p>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </section>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <!-- Begin Javascript for Charts and Tables -->
    <script type="text/javascript">
        var query_url   = 'http://spreadsheets.google.com/pub?key=<?php get_meta('indicator_spreadsheet_url'); ?>';
        var querybase   = "<?php esc_attr(get_meta('indicator_query','',false)); ?>";
        var calulateAverages = true;
        var currentmid =  '<?php get_meta('indicator_default_measure'); ?>';
        var city        = '<?php echo get_option('wpindicators_setting_benchmark_city'); ?>';
        var county      = '<?php echo get_option('wpindicators_setting_benchmark_county'); ?>';
        ActiveInterface.IndicatorViews.init({calcAvg:calulateAverages,benchmarkCity:city,benchmarkCounty:county});
        var chartsettings = {
            colors:[
                '<?php get_meta('indicator_main_color'); ?>',
                '<?php get_meta('indicator_highlight_color'); ?>',
                '<?php get_meta('indicator_bench_color'); ?>'
            ],
            labels:  '<?php get_meta('indicator_axis_labels'); ?>',
            barsize: '<?php get_meta('indicator_bar_size'); ?>',
            range:   '<?php get_meta('indicator_range'); ?>',
            scale:   '',
            force:   '',
            width:   '<?php get_meta('indicator_width'); ?>',
            height:  '<?php get_meta('indicator_height'); ?>',
            title:   '<?php get_meta('indicator_title'); ?>',
            sort:    '<?php get_meta('indicator_sort'); ?>',
            measureOverrides: <?php echo json_encode(unserialize(get_post_meta(get_the_ID(), 'indicator_measures', true))); ?>
        };
        var tablesettings = {
            showBar: false,
            columnFormat: '<?php get_meta('indicator_column_format'); ?>',
            sort:         '<?php get_meta('indicator_sort'); ?>',
            rowFormat: ""
        };
        google.load("visualization", "1", { packages: ['table', 'corechart', 'imagelinechart'] });
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var querystr = querybase.replace("?",currentmid).replace("&lt;","<").replace("&gt;",">");
            var query = new google.visualization.Query(query_url);
            query.setQuery(querystr);
            query.setTimeout(60);
            query.send(function(response) {
                if (!response.isError()) {
                    ActiveInterface.IndicatorViews.dataTables["query"] = response.getDataTable();
                    <?php if ($viewType == 'raw table') : ?>
                    ActiveInterface.IndicatorViews.showRawTable("query","table_div",null,tablesettings,chartsettings,currentmid);
                    <?php elseif($viewType == 'raw table line') : ?>
                    ActiveInterface.IndicatorViews.showRawTable("query","table1_div","table2_div",tablesettings,chartsettings,currentmid,"std");
                    <?php elseif($viewType == 'raw table index line') : ?>
                    ActiveInterface.IndicatorViews.showRawTable("query","table1_div","table2_div",tablesettings,chartsettings,currentmid,"index");
                    <?php elseif($viewType == 'change table') : ?>
                    ActiveInterface.IndicatorViews.showChangeTable("query","table_div",null,tablesettings,chartsettings,currentmid);
                    <?php elseif($viewType == 'change table line') : ?>
                    ActiveInterface.IndicatorViews.showChangeTable("query","table1_div","table2_div",tablesettings, chartsettings,currentmid);
                    <?php elseif($viewType == 'imagechart and table') : ?>
                    ActiveInterface.IndicatorViews.showStandardChart("query","chart_div", chartsettings, currentmid);
                    ActiveInterface.IndicatorViews.showStandardTable("query","table_div", tablesettings);
                    <?php elseif($viewType == 'barchart and table') : ?>
                    ActiveInterface.IndicatorViews.showStandardBarChart("query","chart_div", chartsettings);
                    ActiveInterface.IndicatorViews.showStandardTable("query","table_div", tablesettings);
                    <?php elseif($viewType == 'columnchart years months') : ?>
                    ActiveInterface.IndicatorViews.showYearMonthColumnChart("query","chart_div", chartsettings);
                    <?php elseif($viewType == 'packed columnchart') : ?>
                    ActiveInterface.IndicatorViews.showPackedColumnChart("query","chart_div", chartsettings);
                    <?php elseif($viewType == 'pgh vs bench by year') : ?>
                    ActiveInterface.IndicatorViews.showPghVsBenchChart("query","chart_div","table_div",chartsettings,tablesettings);
                    <?php elseif($viewType == 'month year linechart') : ?>
                    ActiveInterface.IndicatorViews.showMonthYearLineChart("query","chart_div","table_div",chartsettings,tablesettings);
                    <?php elseif($viewType == 'percent change') : ?>
                    ActiveInterface.IndicatorViews.showChange("query","chart_div","table_div",chartsettings,tablesettings,"pct", currentmid);
                    <?php elseif($viewType == 'absolute change') : ?>
                    ActiveInterface.IndicatorViews.showChange("query","chart_div","table_div",chartsettings,tablesettings,"abs", currentmid)
                    <?php elseif($viewType == 'absolute and percent change') : ?>
                    ActiveInterface.IndicatorViews.showChange("query","chart_div","table_div",chartsettings,tablesettings,"both", currentmid);
                    <?php elseif($viewType == 'value chart percent table') : ?>
                    ActiveInterface.IndicatorViews.showChange("query","chart_div","table_div",chartsettings,tablesettings,"vchartptable", currentmid);
                    <?php elseif($viewType == 'value chart absolute table') : ?>
                    ActiveInterface.IndicatorViews.showChange("query","chart_div","table_div",chartsettings,tablesettings,"vchartatable", currentmid);
                    <?php endif; ?>
                    jQuery('.indicator-loading').hide();
                    jQuery('#chartmsg').show();
                    jQuery('#indicator-measures').show();
                } else {
                    jQuery('#chartmsg').hide();
                    jQuery('#indicator-measures').hide();
                    jQuery('.indicator-loading').hide();
                    jQuery('.indicator-error').show();
                }
            });

            jQuery('#select-measures').change(function(){
                currentmid = jQuery(this).val();
                drawChart();
            });
        }
    </script>
<?php
get_footer();