<?php
/**
 * Enable Post Type 'Indicators' with custom post meta data
 *
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 *
 */

/**
 * Register Indicators Post Type
 *
 * @since wpindicators 0.1
 */
function add_indicators_post_type()
{
    $args = array(
        'labels' => array(
            'name' => 'Indicators',
            'singular_name' => 'Indicator',
            'add_new' => 'Add New Indicator',
            'add_new_item' => 'Add New Indicator',
            'edit_item' => 'Edit Indicator',
            'new_item' => 'Add New Indicator',
            'view_item' => 'View Indicator',
            'search_items' => 'Search Indicators',
            'not_found' => 'No Indicators Found',
            'not_found_in_trash' => 'No Indicators Found in Trash'
        ),
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array('indicator_categories'),
        'hierarchical' => true,
        'rewrite' => array('slug' => 'indicators/%indicator_categories%', 'with_front' => false, 'hierarchical' => true),
        'menu_icon' => 'dashicons-chart-line',
        'menu_position' => 5,
        'has_archive' => true
    );
    register_post_type('indicators', $args);
    indicator_categories_register();
}
add_action('init', 'add_indicators_post_type');

/**
 * Register Custom Taxonomy Indicator Categories & Sub-Categories
 *
 * @since wpindicators 0.1
 */
function indicator_categories_register() {
    $labels = array(
        'name'                          => 'Indicator Categories',
        'singular_name'                 => 'Indicator Category',
        'search_items'                  => 'Indicator Categories',
        'popular_items'                 => 'Popular Indicator Categories',
        'all_items'                     => 'All Indicator Categories',
        'parent_item'                   => 'Parent Indicator Category',
        'edit_item'                     => 'Edit Indicator Category',
        'update_item'                   => 'Update Indicator Category',
        'add_new_item'                  => 'Add New Indicator Category',
        'new_item_name'                 => 'New Indicator Category',
        'separate_items_with_commas'    => 'Separate indicator categories with commas',
        'add_or_remove_items'           => 'Add or remove indicator categories',
        'choose_from_most_used'         => 'Choose from most used indicator categories'
    );
    $args = array(
        'label'                         => 'Indicator Categories',
        'labels'                        => $labels,
        'public'                        => true,
        'hierarchical'                  => true,
        'show_ui'                       => true,
        'show_in_nav_menus'             => true,
        'show_admin_column'             => true,
        'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array( 'slug' => 'indicators', 'with_front' => false, 'hierarchical' => true),
        'query_var'                     => true
    );
    register_taxonomy( 'indicator_categories', 'indicators', $args );
    add_event_rewrite_rules();
}

/**
 * Add custom mod_rewrite rules for Indicators
 *
 * @since wpindicators 0.1
 */
function add_event_rewrite_rules() {
    // Debug
    //flush_rewrite_rules();

    // http://localhost/indicators/
    add_rewrite_rule('indicators/?$','index.php?post_type=indicators','top');
    add_rewrite_rule('indicators/page/?([0-9]{1,})/?$','index.php?post_type=indicators&paged=$matches[1]','top');

    // http://localhost/indicators/<category>/
    add_rewrite_rule('indicators/([^/]*)/?$','index.php?post_type=indicators&indicator_categories=$matches[1]','top');
    add_rewrite_rule('indicators/([^/]*)/page/?([0-9]{1,})/?$','index.php?post_type=indicators&indicator_categories=$matches[1]&paged=$matches[2]','top');

    // http://localhost/indicators/<category>/<sub-category>/
    add_rewrite_rule('indicators/([^/]*)/([^/]*)/?$','index.php?post_type=indicators&indicator_categories=$matches[2]','top');
    add_rewrite_rule('indicators/([^/]*)/([^/]*)/page/?([0-9]{1,})/?$','index.php?post_type=indicators&indicator_categories=$matches[2]&paged=$matches[3]','top');

    // http://localhost/indicators/<category>/<sub-category>/<post>/
    add_rewrite_rule('indicators/([^/]*)/([^/]*)/([^/]*)/?$','index.php?post_type=indicators&indicator_categories=$matches[2]&name=$matches[3]','top');
}

/**
 * Simple Debugging - to be removed in 0.2
 *
 * @since wpindicators 0.1
 */
function posts_debug($query) {
    echo '<pre>';
    print_r($query);
    echo '</pre>';
    exit;
}
//add_action('pre_get_posts','posts_debug');

/**
 * Change Indicator Permalinks
 *
 * @since wpindicators 0.1
 * @param $link object WordPress Link
 * @param $post object WordPress Post
 * @return mixed WordPress Link
 */
function filter_indicator_links($link, $post)
{
    if ($post->post_type != 'indicators')
        return $link;

    if ($cats = get_the_terms($post->ID, 'indicator_categories'))
        foreach($cats as $cat) {
            if($cat == end($cats))
                $link = str_replace('%indicator_categories%', $cat->slug, $link);
            else
                $link = str_replace('%indicator_categories%', $cat->slug . '/%indicator_categories%', $link);

        }
    return $link;
}
add_filter('post_type_link', 'filter_indicator_links', 10, 2);

/**
 * Remove default category selector for a more optimal drop down-selector
 *
 * @since wpindicators 0.1
 */
function remove_indicators_categories_box() {
    remove_meta_box('indicator_categoriesdiv','indicators','side');
}
add_action('admin_head', 'remove_indicators_categories_box');


/**
 * Add Meta Box
 *
 * @since wpindicators 0.1
 */
function event_meta_box() {
    wp_enqueue_script('indicator-admin', get_template_directory_uri() . '/assets/js/indicator-admin.js', array('jquery','jquery-ui-tabs'), '0.0.1', true);
    add_meta_box('event_details', 'Indicator Settings', 'add_event_meta_boxes', 'indicators', 'normal', 'high');
}
add_action('add_meta_boxes', 'event_meta_box');


/**
 * Meta Box HTML
 *
 * @param $post object WordPress Post
 * @since wpindicators 0.1
 */
function add_event_meta_boxes($post)
{
    wp_nonce_field('indicator_meta_box', 'indicator_meta_box_nonce'); ?>
    <div id="mytabs" class="inside">
        <ul class="category-tabs">
            <li class="tabs">
                <a href="#indicator_basic">Basic Information</a>
            </li>
            <li class="hide-if-no-js">
                <a href="#indicator_measures">Measures</a>
            </li>
            <li class="hide-if-no-js">
                <a href="#indicator_sources">Source(s)</a>
            </li>
        </ul>
        <br class="clear" />
        <!-- Basic Indicator Information Tab -->
        <div id="indicator_basic" class="tabs-panel" style="margin-top: -18px; border: 1px solid #dfdfdf; padding: 0 25px;">
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row">Title:</th>
                    <td><?php fh_create_text('indicator_title', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Category:</th>
                    <?php
                    $sel_cat = wp_get_post_terms($post->ID, 'indicator_categories', array( 'parent' => 0 ));
                    if($sel_cat)
                        $sel_sub = wp_get_post_terms($post->ID, 'indicator_categories', array( 'parent' => $sel_cat[0]->term_id ));
                    else
                        $sel_sub = false;
                    ?>
                    <td data-action="<?php echo admin_url('admin-ajax.php'); ?>" data-selected-sub="<?php echo $sel_sub ? $sel_sub[0]->term_id : ''; ?>">
                        <?php
                        wp_dropdown_categories(
                            array(
                            'taxonomy'          => 'indicator_categories',
                            'hide_empty'        => 0,
                            'orderby'           => 'name',
                            'name'              => 'tax_input[indicator_categories][]',
                            'show_option_none'  => '',
                            'selected'          => ($sel_cat ? $sel_cat[0]->term_id : 0),
                            'hierarchical'      => 1,
                            'depth'             => 1,
                            'id'                => 'indicator-category'
                            )
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Sub-Category:</th>
                    <td>
                        <select id="indicator-sub-category" name="tax_input[indicator_categories][]" disabled></select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Spreadsheet Key:</th>
                    <td><?php fh_create_text('indicator_spreadsheet_url', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Query:</th>
                    <td><?php fh_create_text('indicator_query', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">View Type:</th>
                    <td><?php fh_create_text('indicator_view_type', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Show Measures:</th>
                    <td><?php fh_create_dropdown('indicator_show_measures', $post->ID, array('Yes' => 1, 'No' => 0)); ?></td>
                </tr>
                <tr>
                    <th scope="row">Sort Direction:</th>
                    <td>
                        <?php fh_create_dropdown('indicator_sort', $post->ID, array('ASC' => 'asc', 'DESC' => 'desc', 'Label' => 'label')); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Default Measure:</th>
                    <td> <?php fh_create_text('indicator_default_measure', $post->ID, '1', 1); ?></td>
                </tr>
                <tr>
                    <th scope="row">Calculate Average:</th>
                    <td><?php fh_create_dropdown('indicator_calc_average', $post->ID, array('Yes' => 1, 'No' => 0)); ?></td>
                </tr>
                <tr>
                    <th scope="row">Show Bars</th>
                    <td><?php fh_create_dropdown('indicator_show_bars', $post->ID, array('Yes' => 1, 'No' => 0)); ?></td>
                </tr>
                <tr>
                    <th scope="row">Column Format:</th>
                    <td><?php fh_create_text('indicator_column_format', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Main Color:</th>
                    <td><?php fh_create_text('indicator_main_color', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Highlight Color:</th>
                    <td><?php fh_create_text('indicator_highlight_color', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Benchmark Color:</th>
                    <td><?php fh_create_text('indicator_bench_color', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Bar Size:</th>
                    <td><?php fh_create_text('indicator_bar_size', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Axis Labels:</th>
                    <td><?php fh_create_text('indicator_axis_labels', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Range:</th>
                    <td><?php fh_create_text('indicator_range', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Force Use:</th>
                    <td><?php fh_create_dropdown('indicator_force_use', $post->ID, array('Yes' => 1, 'No' => 0)); ?></td>
                </tr>
                <tr>
                    <th scope="row">Width:</th>
                    <td><?php fh_create_text('indicator_width', $post->ID); ?></td>
                </tr>
                <tr>
                    <th scope="row">Height:</th>
                    <td><?php fh_create_text('indicator_height', $post->ID); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- Indicator Measures Tab -->
        <div id="indicator_measures" class="tabs-panel hide-if-no-js" style="margin-top: -18px; border: 1px solid #dfdfdf; padding: 15px 35px;">
            <input type="button" class="indicator-add-new-measure" value="Add New Measure" />
            <br><br>
            <table class="widefat" cellspacing="0">
                <thead style="background: #dfdfdf;">
                    <tr>
                        <th>Number</th>
                        <th>Title</th>
                        <th>Axis Labels</th>
                        <th>Range</th>
                        <th>?</th>
                        <th>?</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id="indicator-measures-table">
                    <tr class="cloner-measure" style="display: none;">
                        <td><input type="number" name="indicator_measures[number][]" class="large-text" /></td>
                        <td><input type="text" name="indicator_measures[title][]" class="large-text" /></td>
                        <td><input type="text" name="indicator_measures[labels][]" class="large-text" /></td>
                        <td><input type="text" name="indicator_measures[range][]" class="large-text" /></td>
                        <td><input type="text" name="indicator_measures[some1][]" class="large-text" /></td>
                        <td><input type="text" name="indicator_measures[some2][]" class="large-text" /></td>
                        <td style="line-height: 27px;">
                            <a href="#" class="indicator-remove-measure">Delete</a>
                        </td>
                    </tr>
                    <?php if($measures = get_post_meta($post->ID, 'indicator_measures', true)) : ?>
                    <?php foreach(unserialize($measures) as $key => $array) : ?>
                        <tr style="display: table-row;">
                            <td><input type="number" name="indicator_measures[number][]" value="<?php echo ltrim($key,'m'); ?>" class="large-text"></td>
                            <td><input type="text" name="indicator_measures[title][]" value="<?php echo $array[0]; ?>" class="large-text"></td>
                            <td><input type="text" name="indicator_measures[labels][]" value="<?php echo $array[1]; ?>" class="large-text"></td>
                            <td><input type="text" name="indicator_measures[range][]" value="<?php echo $array[2]; ?>" class="large-text"></td>
                            <td><input type="text" name="indicator_measures[some1][]" value="<?php echo $array[3]; ?>" class="large-text"></td>
                            <td><input type="text" name="indicator_measures[some2][]" value="<?php echo $array[4]; ?>" class="large-text"></td>
                            <td style="line-height: 27px;">
                                <a href="#" class="indicator-remove-measure">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- Indicator Source(s) Tab -->
        <div id="indicator_sources" class="tabs-panel" style="margin-top: -18px; border: 1px solid #dfdfdf; padding: 15px 35px;">
            <input type="button" class="indicator-add-new-source" value="Add New Source" />
            <br><br>
            <table class="widefat" cellspacing="0">
                <thead style="background: #dfdfdf;">
                <tr>
                    <th>Source</th>
                    <th>Link</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="indicator-sources-table">
                <tr class="cloner-source" style="display: none;">
                    <td><input type="text" name="indicator_sources[name][]" class="large-text" /></td>
                    <td><input type="text" name="indicator_sources[link][]" class="large-text" /></td>
                    <td style="line-height: 27px;">
                        <a href="#" class="indicator-remove-source">Delete</a>
                    </td>
                </tr>
                <?php if($sources = get_post_meta($post->ID, 'indicator_sources', true)) : ?>
                    <?php foreach(unserialize($sources) as $array) : ?>
                        <tr style="display: table-row;">
                            <td><input type="text" name="indicator_sources[name][]" value="<?php echo $array[0]; ?>" class="large-text"></td>
                            <td><input type="text" name="indicator_sources[link][]" value="<?php echo $array[1]; ?>" class="large-text"></td>
                            <td style="line-height: 27px;">
                                <a href="#" class="indicator-remove-source">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <small>*If you do not want to create a link, leave the Link blank.</small>
        </div>
    </div>
    <br>
    <?php
}

/**
 * Indicator Settings Meta Box
 *
 * @param $post_id int the Wordpress post ID
 * @since wpindicators 0.1
 */
function indicator_save_post_data($post_id) {

    // Check to see if user has correct privileges
    if (!current_user_can('edit_page', $post_id)) {
        return;
    }
    else {
        if (!current_user_can('edit_post', $post_id))
            return;
    }

    // Check wp_nonce for proper validation
    if (!isset($_POST['indicator_meta_box_nonce']) || !wp_verify_nonce($_POST['indicator_meta_box_nonce'], 'indicator_meta_box'))
        return;

    // Get the Post ID to save the data
    $post_ID      = $_POST['post_ID'];

    // cleanup measures
    $measures = array();
    foreach($_POST['indicator_measures']['number'] as $key => $array) {
        if($key > 0) {
            $measures['m' . $array] = array(
                $_POST['indicator_measures']['title'][$key],
                $_POST['indicator_measures']['labels'][$key],
                $_POST['indicator_measures']['range'][$key],
                $_POST['indicator_measures']['some1'][$key],
                $_POST['indicator_measures']['some2'][$key]
            );
        }
    }

    // cleanup measures
    $sources = array();
    foreach($_POST['indicator_sources']['name'] as $key => $array) {
        if($key > 0) {
            if(!empty($array)) {
                $sources[] = array(
                    $array,
                    $_POST['indicator_sources']['link'][$key],
                );
            }
        }
    }


    // Sanitize user input and update wp_postdata
    update_post_meta($post_ID, 'indicator_spreadsheet_url', $_POST['indicator_spreadsheet_url']);
    update_post_meta($post_ID, 'indicator_query',           $_POST['indicator_query']);
    update_post_meta($post_ID, 'indicator_view_type',       $_POST['indicator_view_type']);
    update_post_meta($post_ID, 'indicator_show_measures',   $_POST['indicator_show_measures']);
    update_post_meta($post_ID, 'indicator_sort',            $_POST['indicator_sort']);
    update_post_meta($post_ID, 'indicator_default_measure', $_POST['indicator_default_measure']);
    update_post_meta($post_ID, 'indicator_calc_average',    $_POST['indicator_calc_average']);
    update_post_meta($post_ID, 'indicator_show_bars',       $_POST['indicator_show_bars']);
    update_post_meta($post_ID, 'indicator_column_format',   $_POST['indicator_column_format']);
    update_post_meta($post_ID, 'indicator_main_color',      $_POST['indicator_main_color']);
    update_post_meta($post_ID, 'indicator_highlight_color', $_POST['indicator_highlight_color']);
    update_post_meta($post_ID, 'indicator_bench_color',     $_POST['indicator_bench_color']);
    update_post_meta($post_ID, 'indicator_bar_size',        $_POST['indicator_bar_size']);
    update_post_meta($post_ID, 'indicator_axis_labels',     $_POST['indicator_axis_labels']);
    update_post_meta($post_ID, 'indicator_range',           $_POST['indicator_range']);
    update_post_meta($post_ID, 'indicator_force_use',       $_POST['indicator_force_use']);
    update_post_meta($post_ID, 'indicator_width',           $_POST['indicator_width']);
    update_post_meta($post_ID, 'indicator_height',          $_POST['indicator_height']);
    update_post_meta($post_ID, 'indicator_title',           $_POST['indicator_title']);
    update_post_meta($post_ID, 'indicator_measures',        serialize($measures));
    update_post_meta($post_ID, 'indicator_sources',         serialize($sources));
}
add_action('save_post_indicators', 'indicator_save_post_data');


/**
 * Form Helper : Create Text Element
 *
 * @since wpindicators 0.1
 * @param string $name - the name of the form element
 * @param int $id - the WordPress post ID
 * @param string $placeholder - placeholder for HTML input[type=text]
 * @param null $default - set the default
 */
function fh_create_text($name='', $id=0, $placeholder='', $default=null)
{
    $value = get_post_meta($id, $name, true);
    echo "<input type='text' id='{$name}' name='{$name}' value=\"{$value}\" placeholder='{$placeholder}' class='large-text' />";
}

/**
 * Form Helper : Create Select Element
 *
 * @since wpindicators 0.1
 * @param string $name - the name of the form element
 * @param int $id - the WordPress post ID
 * @param array $options - array of options for the select, key = label & value = value
 * @param null $default - set the default
 */
function fh_create_dropdown($name='', $id=0, $options=array(), $default=null)
{
    $value = get_post_meta($id, $name, true);
    echo "<select id='{$name}' name='{$name}'>";
        foreach($options as $lab => $val) {
            echo "<option value='{$val}' " . ($value == $val ? " selected='selected'" : "") . ">{$lab}</option>";
        }
    echo "</select>";
}


/**
 * Create a Hook to be used for AJAX calls using 'indicator_select_category' action
 *
 * @since wpindicators 0.1
 * @link https://codex.wordpress.org/AJAX_in_Plugins
 */
function indicator_select_category_callback()
{
    if (!isset($_POST['selected']) || empty($_POST['selected'])) {
        header("HTTP/1.1 400 Bad Request");
        exit;
    }

    $selected = intval($_POST['selected']);
    $response = get_categories(
        array(
            'taxonomy'          => 'indicator_categories',
            'hide_empty'        => 0,
            'orderby'           => 'name',
            'child_of'          => $selected
        )
    );

    if(!$response) {
        header("HTTP/1.1 400 Bad Request");
        exit;
    }

    header('Content-type: application/json;');
    echo json_encode($response);
    exit;

}
add_action('wp_ajax_indicator_select_category', 'indicator_select_category_callback');