<?php
/**
 * Custom Theme Settings Admin Page
 *
 * @package WordPress
 * @subpackage wpindicators
 * @since wpindicators 0.1
 */
class ThemeSettings {

    /**
     * Creates the 'Theme Settings' Menu Item under 'Appearance'
     *
     * @since wpindicators 0.1
     */
    function __construct() {
        add_submenu_page(
            'themes.php', 'Theme Settings', 'Theme Settings', 'manage_options',
            'theme-settings', array($this, 'theme_settings_page')
        );
    }

    /**
     * Load the Theme Settings Page's HTML/Submit Page
     *
     * @since wpindicators 0.1
     */
    function theme_settings_page() {
        if (!current_user_can('manage_options')) {
            wp_die('You do not have sufficient permissions to access this page.');
        } else {
            if (isset($_POST["security"])) {
                $this->save_theme_settings_page();
            } else {
                $this->theme_settings_page_form();
            }
        }
    }

    /**
     * Easily create form input[type=text] elements
     *
     * @param string $label
     * @param string $option
     * @param string $placeholder
     * @param int $size
     * @since wpindicators 0.1
     */
    function theme_settings_form_text($label, $option, $placeholder='', $size=25) {
        ?>
        <tr valign="top">
            <th scope="row">
                <label for="<?php echo "s[{$option}]"; ?>"><?php echo $label; ?>:</label>
            </th>
            <td>
                <input type="text"
                       value="<?php echo get_option($option); ?>"
                       name="<?php echo "s[{$option}]"; ?>"
                       size="<?php echo $size; ?>"
                       placeholder="<?php echo $placeholder; ?>"
                />
            </td>
        </tr>
        <?php
    }

    /**
     * Easily create form image upload elements
     *
     * @param string $label
     * @param string $option
     * @since wpindicators 0.1
     */
    function theme_settings_form_image($label, $option) {
        ?>
        <tr valign="top">
            <th scope="row">
                <label for="<?php echo "s[{$option}]"; ?>"><?php echo $label; ?>:</label>
            </th>
            <td>
                <input type="text"
                       id="<?php echo $option; ?>"
                       value="<?php echo get_option($option); ?>"
                       name="<?php echo "s[{$option}]"; ?>"
                       size="80"
                       placeholder=""
                       readonly="readonly"
                    />
                <input class="media-selector button" type="button" data-target="<?php echo $option; ?>" value="Select an Image" />
            </td>
        </tr>
        <?php
    }

    /**
     * Load the Theme Settings Page's HTML
     *
     * @since wpindicators 0.1
     */
    function theme_settings_page_form() {  ?>
        <br><h1>Theme Settings</h1><hr>
        <form method="POST" action="">
            <h3>Basic Information:</h3>
            <input type="hidden" name="update_settings" value="Y" />
            <?php wp_nonce_field('theme-settings','security'); ?>
            <table class="form-table">
                <?php $this->theme_settings_form_text('Site Title','blogname','My Website!',50); ?>
                <?php $this->theme_settings_form_text('Tagline/Slogan','blogdescription','Just another WordPress site!',80); ?>
                <?php $this->theme_settings_form_image('Small Logo','wpindicators_setting_small_logo'); ?>
                <?php $this->theme_settings_form_image('Medium Logo','wpindicators_setting_medium_logo'); ?>
                <?php $this->theme_settings_form_text('Google Analytics ID','wpindicators_setting_google_analytics','UA-000000-01',15); ?>
                <?php $this->theme_settings_form_text('Benchmark City','wpindicators_setting_benchmark_city','Pittsburgh',40); ?>
                <?php $this->theme_settings_form_text('Benchmark County','wpindicators_setting_benchmark_county','Allegheny',40); ?>
            </table>
            <br>
            <h2>Footer Icon Links:</h2>
            <table class="form-table">
                <?php $this->theme_settings_form_text('Twitter Username','wpindicators_setting_twitter_username','UCSUR'); ?>
                <?php $this->theme_settings_form_text('Facebook Username','wpindicators_setting_facebook_username','UCSUR'); ?>
                <?php $this->theme_settings_form_text('Google+ Username','wpindicators_setting_googleplus_username','UCSUR'); ?>
                <?php $this->theme_settings_form_text('GitHub Username','wpindicators_setting_github_username','UCSUR-Pitt'); ?>
                <?php $this->theme_settings_form_text('Contact URL','wpindicators_setting_contact_url','http://localhost/contact', 50); ?>
            </table>
            <br>
            <h2>Twitter API:</h2>
            <table class="form-table">
                <?php $this->theme_settings_form_text('Access Token','wpindicators_setting_twitter_api_at','',55); ?>
                <?php $this->theme_settings_form_text('Access Token Secret','wpindicators_setting_twitter_api_ats','',55); ?>
                <?php $this->theme_settings_form_text('Consumer Key','wpindicators_setting_twitter_api_ck','',55); ?>
                <?php $this->theme_settings_form_text('Consumer Secret','wpindicators_setting_twitter_api_cs','',55); ?>
            </table>
            <br>
            <h2>MailChimp API:</h2>
            <table class="form-table">
                <?php $this->theme_settings_form_text('API Key','wpindicators_setting_mailchimp_api','abc123abc123abc123abc123abc123',55); ?>
                <?php $this->theme_settings_form_text('List ID','wpindicators_setting_mailchimp_list','b1234346',55); ?>
            </table>
            <p>
                <input type="submit" value="Save Settings" class="button-primary"/>
            </p>
        </form>
    <?php }

    /**
     * Updates the WP Options with the POSTed Data
     *
     * @since wpindicators 0.1
     */
    function save_theme_settings_page() {
        $response = 'An error has occurred. Please try again.';
        if(isset($_POST['security'])) {
            if (wp_verify_nonce($_POST['security'], 'theme-settings')) {

                foreach($_POST['s'] as $setting => $value) {
                    update_option($setting, esc_attr($value), 'yes');
                }

                // set option to retrieve tweets now
                update_option('wpindicators_setting_twitter_nextpull', time() - 1);

                $response = 'Settings successfully saved!';
            }
        }
        //output response ?>
        <h3><?php echo $response; ?></h3>
        <a href="#" onClick="window.location.reload()">Go Back</a>
        <?php
    }
}